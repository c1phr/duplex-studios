$(document).ready(function () {
	if ($.browser.mozilla || $.browser.msie) {
		$("body").css("overflow", "visible");
	}
});

$(window).load(function () {
	$("#bg_scene").delay(1000).fadeIn(2000);
	$(".head_text").delay(3000).slideDown("slow");
	$(".button_field").delay(3500).fadeIn(2000);
});