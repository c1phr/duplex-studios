function springBoard() {
    var screenWidth = $(window).width();
    if (screenWidth == 480) {
        $('.springBoard_item').css({'padding':'24px'});
    }
    if (screenWidth == 320) {
        $('.springBoard_item').css({'padding':'15px'});
    }
    else {
        $('.springBoard_item').css({'padding-left':(screenWidth * 0.05) + 'px'});
    }

}
//Emulate document.ready() for Jquery Mobile's Ajax loading
$('#portfolio').live('pageinit', function (event) {
    springBoard();
});

$(window).resize(function () {
    springBoard();
});