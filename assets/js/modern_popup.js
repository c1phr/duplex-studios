var popupStatus = 0;

function loadPopup() {
    if (popupStatus == 0) {
        $("#backgroundPopup").css({
            "opacity":"0.7"
        });
        $("#backgroundPopup").fadeIn("slow");
        $("#popup").fadeIn("slow");
        popupStatus = 1;
    }
}

function disablePopup() {
    if (popupStatus == 1) {
        $("#backgroundPopup").fadeOut("slow");
        $("#popup").fadeOut("slow");
        popupStatus = 0;
    }
}

function centerPopup() {
    var windowWidth = document.documentElement.clientWidth;
    var windowHeight = document.documentElement.clientHeight;
    var popupHeight = $("#popup").height();
    var popupWidth = $("#popup").width();
    $("#popup").css({
        "position":"absolute",
        "top":windowHeight / 2 - popupHeight / 2,
        "left":windowWidth / 2 - popupWidth / 2
    });
}

$(document).ready(function () {
    $(".button").click(function () {
        centerPopup();
        loadPopup();
    });
    $("#popupClose").click(function () {
        disablePopup();
    });
    $(document).keypress(function (e) {
        if (e.keyCode == 27 && popupStatus == 1) {
            disablePopup();
        }
    });

});
